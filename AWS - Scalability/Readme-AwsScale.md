# Readme file for AWS - Scalability #

The project files in the folder **AWS - Scalability** is used to monitor the scalibity on EC2 instances from ElasticBeanStalk applications

### Amazon Web Services Scalability monitoring ###

* Login/SignUp using AWS DynamoDB
* Upload of files to S3 on login
* Monitoring the upload on Elasticbeanstalk for scalability

### Set Up ###

* Create AWS account
* Enter the AWS Access Key/Secret Key
* Zip the files and upload as Elasticbeanstalk application

### Developed By: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)