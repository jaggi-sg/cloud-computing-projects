# Cloud Computing Projects #

This repository contains projects related to Cloud Computing.

### Cloud Computing Projects ###

* Amazon Web Services - S3, EC2, RDS, DynamoDB, Elasticbeanstalk
* Google App Engine - Storage, DataStore
* Hadoop
* Machine Learning - Clustering, Visualization (Weka, cloud9.io)
* Dropbox

### Developed by: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)