# Readme file for ML and Hadoop on NCDC data #

The project files in the folder ** ML and Hadoop on NCDC data ** is used to perform machine learning and big data analysis on NCDC data

### Hadoop on NCDC data ###

* Setup Hadoop (on Amazon EC2 or local machine)
* Start hadoop and run the java files in *Big Data Processing - Hadoop* folder (Read the **How to Run Hadoop** file for help)
* Yearly average extreme temperature is obtained for each station

### Machine Learning - Clustering ###

* Clustering is applied on interesting information on the NCDC data
* Setup Weka jar files for clustering
* Run the java files *Machine Learning - Clustering* folder on the arff file
* Using k-means clustering, Clusters of data is obtained

### Machine Learning - Visualization ###

* Setup AWS account to do manual visualization of files from S3.
* Run the html files on the required files from the folder *Machine Learning - Visualization*
* Sample visualizations can be viewed at [Bubble-Chart](https://virtualization-jaggi-sg.c9.io/visualize-2.html) and [Scatter-Plot](https://virtualization-jaggi-sg.c9.io/visualize.html)


### Developed By: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)