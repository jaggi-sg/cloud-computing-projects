/*
Jagadish Shivanna
1001050680
References:
http://weka.wikispaces.com/Converting+CSV+to+ARFF
http://weka.wikispaces.com/Use+Weka+in+your+Java+code#Clustering
http://www.dbs.ifi.lmu.de/~zimek/diplomathesis/implementations/EHNDs/doc/weka/clusterers/ClusterEvaluation.html
*/

package clusterWeka;

import java.io.BufferedReader;
import java.io.FileReader;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveWithValues;

public class Cluster3Attributes {

	public static void main(String[] args) throws Exception {

		BufferedReader breader = new BufferedReader(new FileReader("/home/ubuntu/clusterWeka/505720.arff"));

		Instances train = new Instances(breader);
		breader.close();

		//Removing unwanted values in EMXT (Extreme Max temperature column) - in column 12
		RemoveWithValues filter12 = new RemoveWithValues();
		String[] options12 = new String[4];
		options12[0] = "-C";   //Select class attribute from dataset
		options12[1] = "12";   //class attribute from dataset that has EMXT data 
		options12[2] = "-S";  //Match for smaller than values
		options12[3] = "0";   //If less than zero remove 
		filter12.setOptions(options12);
		filter12.setInputFormat(train);
		Instances newData12 = Filter.useFilter(train, filter12);
				
		//Removing unwanted values in MMXT (Monthly Mean maximum temperature ) - in column 14
		RemoveWithValues filter14 = new RemoveWithValues();
		String[] options14 = new String[4];
		options14[0] = "-C";   //Select class attribute from dataset
		options14[1] = "14";    //class attribute from dataset that has EMXT data 
		options14[2] = "-S";   //Match for smaller than values
		options14[3] = "0";    //If less than zero remove
		filter14.setOptions(options14);
		filter14.setInputFormat(newData12);
		Instances newData14 = Filter.useFilter(newData12, filter14);

		//Removing unwanted values in MNTM (Monthly mean temperature ) - in column 16
		RemoveWithValues filter16 = new RemoveWithValues();
		String[] options16 = new String[4];
		options16[0] = "-C";   //Select class attribute from dataset
		options16[1] = "14";    //class attribute from dataset that has EMXT data 
		options16[2] = "-S";   //Match for smaller than values
		options16[3] = "0";    //If less than zero remove
		filter16.setOptions(options16);
		filter16.setInputFormat(newData14);
		Instances newData16 = Filter.useFilter(newData14, filter16);
				
		//Removing unwanted attributes before clustering
		String[] options = new String[2];
		options[0] = "-R"; // "range"
		options[1] = "1,2,3,4,5,6,7,8,9,10,11,13,15,17"; //Select attributes 12 and 14
		Remove remove = new Remove(); 
		remove.setOptions(options); 
		remove.setInputFormat(newData14);
		Instances newData = Filter.useFilter(newData16, remove);

		// --- Do simple K means Clustering
		SimpleKMeans kmeans = new SimpleKMeans();
		kmeans.setPreserveInstancesOrder(true);
		kmeans.setMaxIterations(100);
		kmeans.setNumClusters(6);
		kmeans.buildClusterer(newData);

		// --- Evaluate the clustering
		ClusterEvaluation ceval = new ClusterEvaluation();
		ceval.setClusterer(kmeans);
		ceval.evaluateClusterer(newData);

		System.out.println(ceval.getRevision());
		System.out.println(ceval.clusterResultsToString());

		//View the clustering assignments
		/* int[] assignments = kmeans.getAssignments();
		 int i=0;
		 for(int clusterNum : assignments) {
		 System.out.printf("Instance %d -> Cluster %d \n ", i, clusterNum);
		 i++;
		} */
	}
}
