/* Jagadish Shivanna
1001050680 */

package Weatherdata;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.io.Text;
import java.io.IOException;
import java.util.*;

public class WeatherReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter)
throws IOException {
	int temperature = 0;
	int count = 0;
	while (values.hasNext()) {
		  IntWritable value = (IntWritable) values.next(); //Get temperature value
		  temperature += value.get();
		  count+=1;
      	}
	output.collect(key, new IntWritable(temperature/count)); //Output average of EMXT
	}
}
