/*Jagadish Shivanna
1001050680*/

package Weatherdata;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import java.io.IOException;
import java.util.StringTokenizer;

public class WeatherMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

public static final int UNKNOWN = -9999;

public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
		String dataset = value.toString(); //Read dataset
		String[] datavalues = dataset.split(","); //Split by comma
		int temperature = Integer.parseInt(datavalues [12]); //Select column from dataset to map (EMXT column)
		if(temperature != UNKNOWN){
		    	output.collect(new Text(datavalues [2]), new IntWritable(temperature));
		}
      }
}