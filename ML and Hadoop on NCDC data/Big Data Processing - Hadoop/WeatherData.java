/*Jagadish Shivanna
1001050680 */

package Weatherdata;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import java.net.InetSocketAddress;

public class WeatherData {

public static void main(String[] args) {

	JobClient client = new JobClient(); //Create Job
	JobConf conf = new JobConf(Weatherdata.WeatherData.class);
		conf.setNumMapTasks(10); //Set Mappers
		conf.setNumReduceTasks(2); //Set Reducers

		conf.setOutputKeyClass(Text.class); //Year
		conf.setOutputValueClass(IntWritable.class); //Average Temperature
	
conf.setInputFormat(org.apache.hadoop.mapred.TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		conf.setMapperClass(WeatherDataMapper.class);
		conf.setReducerClass(WeatherDataReducer.class);
		conf.setCombinerClass(WeatherDataReducer.class);

		client.setConf(conf);

		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
