import MySQLdb
import timeit

conn = MySQLdb.connect(
	host="173.194.109.85",
	user="root",
	passwd="root",
	db="clouddb")
print conn
cur = conn.cursor()
start = timeit.default_timer()
cur.execute("SELECT * from weatherdata LIMIT 100000;")
#print cur.fetchall()
end = timeit.default_timer()
print "SQL QUERY TIME: ", end-start