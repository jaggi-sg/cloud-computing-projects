<!-- 
Jagadish Shivanna
1001050680
References:
https://cloud.google.com/storage/docs/json_api/v1/json-api-php-samples
http://www.zdatatech.com/blog/php-upload-to-google-cloud-storage-from-app-engine
https://cloud.google.com/appengine/docs/php/googlestorage/
-->
<?php 
@ob_start();
session_start();
if(isset($_SESSION['username'])) {
echo "User:", $_SESSION['username'];
}?>
<?php
error_reporting ( E_ALL );
ini_set ( 'display_errors', 'On' );
//require_once 'C:\Program Files (x86)\Google\google_appengine\php\sdk\google\appengine\api\cloud_storage\CloudStorageTools.php';
//use google\appengine\api\CloudStorageTools;
require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
use google\appengine\api\cloud_storage\CloudStorageTools;
use \google\appengine\api\mail\Message;
$options = [ 'gs_bucket_name' => 'jaggisg3836' ];
$upload_url = CloudStorageTools::createUploadUrl('/upload', $options);
?>
<html>
<head>
<title>GAE Upload</title>
<style>
.btn {
	background-color: #745656;
	-webkit-border-radius: 5px;
	color: #fff;
	font-family: 'Palatino Linotype';
	font-size: 16px;
	border: 1px solid #745656;
}
</style>
</head>
<body style="background-color: #A9A9A9; font-family: Trebuchet MS; font-size: 16;">
<?php 
if(isset($_POST['submit'])) {
if(isset($_POST['upload']) AND $_POST['upload'] == "yes"){
$time_pre = microtime(true);
	$filename = $_FILES['fileUpload']['name'];
	//echo $filename;
	$bucket = 'gs://jaggisg3836/';
   $gs_name = $_FILES['fileUpload']['tmp_name'];
   move_uploaded_file($gs_name, 'gs://jaggisg3836/'.$filename.'');
   $object_public_url = CloudStorageTools::getPublicUrl('gs://jaggisg3836/'.$filename.'', false);
   //echo $object_public_url;
   $time_post = microtime(true);
   ?>
  <?php echo "<b>Uploaded File: </b>" ?> <a href="<?= $object_public_url ?>"><?= $object_public_url ?></a>
     <?php   
	 $filesize = filesize('gs://jaggisg3836/'.$filename.'')/1024;
	 echo '<br/>';
	 echo '<b>File Size: </b>', $filesize, 'Kilobytes';
	echo '<br/>';
	$exec_time = number_format(($time_post - $time_pre), 2);
	$_SESSION['mailtime'] = $exec_time;
	echo '<b>Time to upload: </b> (in micro secs) ', ($time_post-$time_pre);
	echo '<br>Date: ', date ("F d Y H:i:s.", filemtime('gs://jaggisg3836/'.$filename.''));
	//echo $_SESSION['mailtime'];
	
	$body = "Hi,<br> This is the output of file upload from Google Storage <br> Uploaded File: ".$object_public_url."<br>File Size: ".$filesize."(in KB's)<br>Time: ".($time_post-$time_pre)."(in micro secs)<br>Regards,<br> Jagadish Shivanna <br> 1001050680<br> Sent from http://jaggi-sg30.appspot.com/";

         $mailoptions = [
             "sender" => "jagadish030@gmail.com",
             "to" => "cloudatuta@gmail.com",
             "subject" => "PA 6 Output - 1001050680",
             "htmlBody" => $body
      ];

    $message = new Message($mailoptions);
    $message->send();
	
	 }
} 

if(isset($_POST['logout'])){ 
unset ( $_SESSION ['username'] );
echo("<script>location.href = '/login.php';</script>");
}
//if(isset($_POST['delete'])){ 
//unlink();
//}
?>

<form action="<?php echo $upload_url?>" enctype="multipart/form-data" method="post">
  <p><b>Files to upload: </b></p> <br>
  <input type="hidden" name="upload" value="yes">
  <input type="file" name="fileUpload" >
  <input type="submit" value="Upload" name="submit" class="btn">
  <button type="submit" name="mail" class="btn">Mail</button>
  <button type="submit" name="logout" id="logout" style="float: right;" class="btn">Logout</button>
  </form>
<!-- </form>
<img src="<?= $object_public_url ?>"/>
<form> -->
<?php
if(isset($_POST['mail'])) {
echo "Mail Clicked";
  }
$dir = 'gs://jaggisg3836/';
$files1 = scandir($dir); 
echo "<br><br>Files in bucket:";?>
 <form action="upload.php" method="POST">
 <ul>
<?php foreach($files1 as $file) { ?>
 <li><?= $file ?></li>
  <!-- <input type="submit" value="delete" name="delete" class="btn"> -->
 <?php 
 //echo filectime($file);
 //echo "<img src=".CloudStorageTools::getPublicUrl($dir$filename.'', false);."/>";
 echo "<br>";
} ?>
</ul>
</form>
</body>
</html>