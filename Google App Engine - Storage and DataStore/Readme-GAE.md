# Readme file for Google App Engine - Storage and DataStore #

The project files in the folder **Google App Engine - Storage and DataStore ** is used to monitor uploading on *GAE- Storage*,
and insertion and querying on *GAE- DataStore*

### Google App Engine - Storage and DataStore monitoring ###

* Upload to Storage
* Insertion and query using DataStore on NCDC data
* Monitoring on different file sizes

### Set Up ###

* Create GAE account
* Enter the AWS Access Key/Secret Key
* Run the python files for Datastore monitor
* Run the php file (login.php) for Storage monitor

### Developed By: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)