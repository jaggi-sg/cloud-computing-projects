import MySQLdb
import csv
import timeit

conn2 = MySQLdb.connect(
	host="173.194.109.85",
	user="root",
	passwd="root",
	db="clouddb")

print conn2
cur = conn2.cursor()
cur.execute("DROP TABLE IF EXISTS weatherdata;")
cur.execute("CREATE TABLE IF NOT EXISTS weatherdata (station VARCHAR(40), station_name VARCHAR(100), date VARCHAR(20), mmxp VARCHAR(20), mmnp VARCHAR(20), tevp VARCHAR(20), emxp VARCHAR(20), mxsd VARCHAR(20), dsnw VARCHAR(20), tpcp VARCHAR(20), tsnw VARCHAR(20), emxt VARCHAR(20), emnt VARCHAR(20), mmxt VARCHAR(20), mmnt VARCHAR(20), mntm varchar(20), twnd varchar(20));")
#cur.execute("CREATE TABLE IF NOT EXISTS testtable (uname VARCHAR(20), ak VARCHAR(20), sk VARCHAR(20));")
cur.execute('DELETE FROM weatherdata;')
f = open('E:/UTA/Spring 2015/Cloud Computing/PA 6/505720 - 10K.csv', 'rb')
#f = open('E:/UTA/Spring 2015/Cloud Computing/PA 6/505720 - 25K.csv', 'rb')
#f = open('E:/UTA/Spring 2015/Cloud Computing/PA 6/505720 - 100K.csv', 'rb')
csvfile = csv.reader(f)
#csvfile = csv.reader(file('505720.csv'))
csvfile.next()
start = timeit.default_timer()
print "Start Time:", start
print "Uploading weather data to GAE SQL"
for row in csvfile:
	#sql="INSERT INTO weatherdata VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
	cur.execute("INSERT INTO weatherdata VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",row)
	#cur.execute('INSERT INTO testtable VALUES(%s,%s,%s);',row)
	#print row
f.close()
conn2.commit()
end = timeit.default_timer()
print "End Time: ", end
print "Upload complete..."
time = end-start
cur.execute('SELECT count(*) FROM weatherdata') 
print "Time for upload to Google SQL: ",time
print "No of rows uploaded: ", cur.fetchall()

# Upload complete...
# Time for upload to RDS:  790.685754997
# No of rows uploaded:  ((8790L,),)