import boto
import boto.s3.connection
from boto.s3.key import Key
import timeit

ACCESS_KEY_ID = 'Your-access-key'
SECRET_ACCESS_KEY = 'Your-secret-key'
bucketname='Your-bucket-name'

# conn = boto.s3.connect_to_region('s3-us-west-2.amazonaws.com',
       # aws_access_key_id=ACCESS_KEY_ID,
       # aws_secret_access_key=SECRET_ACCESS_KEY,
       # is_secure=False,
       # calling_format = boto.s3.connection.OrdinaryCallingFormat(),
       # )
	   
conn = boto.connect_s3(
        aws_access_key_id = ACCESS_KEY_ID,
        aws_secret_access_key = SECRET_ACCESS_KEY,
        is_secure=False)
		
print conn
rs = conn.get_all_buckets()
for bs in rs:
	print "Buckets in my s3: ",bs.name
bucket = conn.get_bucket(bucketname)
bucketKey = Key(bucket)
bucketKey.key = 'earthquake_data.csv'
upstart = timeit.default_timer()
bucketKey.set_contents_from_filename('quakedata.csv')
upstop = timeit.default_timer()
print "Time for upload to s3: ",upstop-upstart
#mybucket = conn.get_bucket('jaggisg3836')
#mybucket.list()
bucketKey.key = 'earthquake_data.csv'
downstart = timeit.default_timer()
bucketKey.get_contents_to_filename('quakedatafroms3.csv')
downstop = timeit.default_timer()
print "Time for download from s3: ",downstop-downstart