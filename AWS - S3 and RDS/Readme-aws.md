# Readme file for AWS - S3 and RDS #

The project files in the folder **AWS - S3 and RDS** is used to monitor uploading on *AWS S3*, and insertion and querying on *AWS RDS* using Memcached technique

### Amazon Web Services S3/RDS monitoring ###

* S3 Upload
* RDS insertion and query using MySQL on Earthquake data
* RDS insertion and query using Memcached technique on Earthquake data

### Set Up ###

* Create AWS account
* Enter the AWS Access Key/Secret Key
* Run the python files

### Developed By: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)